package com.example.demoDTO;

import com.example.demoDTO.dto.questionDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDtoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDtoApplication.class, args);
	}

}
