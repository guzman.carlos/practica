package com.example.demoDTO.rest;

import com.example.demoDTO.dto.questionDTO;
import com.example.demoDTO.models.question;
import com.example.demoDTO.service.questionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/question")
public class questionRest {

    @Autowired
    private questionServiceImpl questionService;

    @GetMapping("/getAllQuestion")
    private ResponseEntity<List<questionDTO>> getAllQuestion(){
        return ResponseEntity.ok(questionService.getAllQuestion());
    }
}
