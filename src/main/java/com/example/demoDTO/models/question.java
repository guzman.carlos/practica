package com.example.demoDTO.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "question")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idQuestion;

    @Column(name = "question")
    private String question;

}