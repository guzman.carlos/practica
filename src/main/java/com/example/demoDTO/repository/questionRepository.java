package com.example.demoDTO.repository;

import com.example.demoDTO.models.question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface questionRepository extends JpaRepository<question, Long> {

}