package com.example.demoDTO.service;

import com.example.demoDTO.dto.questionDTO;
import com.example.demoDTO.models.question;

import java.util.List;

public interface questionService {
    public List<questionDTO> getAllQuestion();
}