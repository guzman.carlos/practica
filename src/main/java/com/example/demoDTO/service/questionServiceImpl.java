package com.example.demoDTO.service;

import com.example.demoDTO.dto.questionDTO;
import com.example.demoDTO.models.question;
import com.example.demoDTO.repository.questionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class questionServiceImpl implements questionService{

    @Autowired
    private questionRepository questionRepository;

    @Override
    public List<questionDTO> getAllQuestion() {
        List<question> questionList = questionRepository.findAll();
        ModelMapper modelMapper = new ModelMapper();
        List<questionDTO> questionDTOList = new ArrayList<>();

       // questionList.stream().forEach(e -> questionDTOList.add(modelMapper.map(e, questionDTO.class)));

        questionDTOList = questionList.stream()
                .map(param -> modelMapper.map(param, questionDTO.class))
                .collect(Collectors.toList());

        return questionDTOList;
    }
}